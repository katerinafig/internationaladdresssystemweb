package com.netcracker.practice.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netcracker.practice.model.AddressUnitType;

import java.io.IOException;
import java.util.UUID;

public class AddressUnitTypeDeserializer extends JsonDeserializer<AddressUnitType> {
    @Override
    public AddressUnitType deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        AddressUnitType result = new AddressUnitType();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode addressUnitTypeId = ((ObjectNode) treeNode).findValue("addressUnitTypeId");
        if(addressUnitTypeId!=null)
        result.setAddressUnitTypeId(UUID.fromString(addressUnitTypeId.asText()));
        JsonNode name = ((ObjectNode) treeNode).findValue("name");
        if(name!=null) result.setName(name.asText());
        JsonNode description = ((ObjectNode) treeNode).findValue("description");
        if(description!=null) result.setDescription(description.asText());
        JsonNode parentType = ((ObjectNode)treeNode).findValue("parentType");
        if(parentType!=null) result.setParentType(UUID.fromString(parentType.asText()));
        return result;

    }
}
