package com.netcracker.practice.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netcracker.practice.model.CharacteristicValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.util.UUID;

public class CharacteristicValueDeserializer extends JsonDeserializer<CharacteristicValue> {
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public CharacteristicValue deserialize(JsonParser jsonParser,
                                           DeserializationContext deserializationContext) throws IOException,
            JsonProcessingException {
        CharacteristicValue result = new CharacteristicValue();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode valueId = ((ObjectNode) treeNode).findValue("valueId");
        if (valueId != null)
            result.setValueId(UUID.fromString(valueId.asText()));
        JsonNode name = ((ObjectNode) treeNode).findValue("name");
        if (name != null) result.setName(name.asText());
        JsonNode description = ((ObjectNode) treeNode).findValue("description");
        if (description != null) result.setDescription(description.asText());
        return result;
    }
}
