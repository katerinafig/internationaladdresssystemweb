package com.netcracker.practice.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netcracker.practice.model.AddressUnitValue;
import com.netcracker.practice.model.CharacteristicValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

public class AddressUnitValueDeserializer extends JsonDeserializer<AddressUnitValue> {
    @Autowired
    ObjectMapper objectMapper;
    @Override
    public AddressUnitValue deserialize(JsonParser jsonParser,
                                        DeserializationContext deserializationContext) throws IOException,
            JsonProcessingException {
        AddressUnitValue result = new AddressUnitValue();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        result.setCharacterValue( objectMapper.readValue(((ObjectNode)treeNode)
                .findValue("characterValue").toString(),CharacteristicValue.class));
        JsonNode value = ((ObjectNode)treeNode).findValue("value");
        if(value!=null)result.setValue(value.asText());
        return result;
    }
}
