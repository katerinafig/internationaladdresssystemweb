package com.netcracker.practice.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netcracker.practice.model.AddressUnitRecord;
import com.netcracker.practice.model.AddressUnitType;
import com.netcracker.practice.model.AddressUnitValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.util.Set;
import java.util.SortedSet;
import java.util.UUID;

public class AddressUnitRecordDeserializer extends JsonDeserializer<AddressUnitRecord> {
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public AddressUnitRecord deserialize(JsonParser jsonParser,
                                         DeserializationContext deserializationContext) throws IOException,
            JsonProcessingException {
        AddressUnitRecord result = new AddressUnitRecord();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode addressUnitRecordId = ((ObjectNode) treeNode).findValue("addressUnitRecordId");
        if (addressUnitRecordId != null)
        result.setAddressUnitRecordId(UUID.fromString(addressUnitRecordId.asText()));
        JsonNode name = ((ObjectNode) treeNode).findValue("name");
        if (name != null)
            result.setName(name.asText());
        JsonNode addressUnit = ((ObjectNode) treeNode).findValue("addressUnit");
        if (addressUnit != null)
            result.setAddressUnit(UUID.fromString(addressUnit.asText()));
        JsonNode parent = ((ObjectNode) treeNode).findValue("parent");
        if (parent != null)
            result.setParent(UUID.fromString(parent.asText()));
        JsonNode nextRecordId = ((ObjectNode) treeNode).findValue("nextRecordId");
        if (nextRecordId != null)
            result.setNextRecordId(UUID.fromString(nextRecordId.asText()));
        JsonNode prevRecordId = ((ObjectNode) treeNode).findValue("prevRecordId");
        if (prevRecordId != null)
            result.setPrevRecordId(UUID.fromString(prevRecordId.asText()));
        if (((ObjectNode) treeNode).findValue("addressUnitType") != null) {
            result.setAddressUnitType(objectMapper.readValue(((ObjectNode) treeNode)
                    .findValue("addressUnitType").toString(), AddressUnitType.class));
        }
        if(((ObjectNode) treeNode).findValue("addressUnitValueList")!=null) {
            Set<AddressUnitValue> values = objectMapper.readValue(((ObjectNode) treeNode)
                    .findValue("addressUnitValueList").toString(), new TypeReference<Set<AddressUnitValue>>() {
            });
            result.setAddressUnitValueList(values);
        }
        return result;
    }
}