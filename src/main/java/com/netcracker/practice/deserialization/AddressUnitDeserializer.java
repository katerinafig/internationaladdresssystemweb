package com.netcracker.practice.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netcracker.practice.model.AddressUnit;
import com.netcracker.practice.model.AddressUnitRecord;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

public class AddressUnitDeserializer extends JsonDeserializer<AddressUnit> {
    @Autowired
    ObjectMapper objectMapper;
    @Override
    public AddressUnit deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        AddressUnit result = new AddressUnit();
        TreeNode treeNode = jsonParser.getCodec().readTree(jsonParser);
        JsonNode addressUnitId = (((ObjectNode) treeNode).findValue("addressUnitId"));
        if (addressUnitId != null)
        result.setAddressUnitGuId(UUID.fromString(addressUnitId.asText()));
        result.setAddressRecordsList(objectMapper.readValue(((ObjectNode) treeNode)
                .findValue("addressRecordsList").toString(),new TypeReference<Set<AddressUnitRecord>>(){}));
        return result;
    }
}
