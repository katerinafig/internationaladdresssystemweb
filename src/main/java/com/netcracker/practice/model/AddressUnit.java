package com.netcracker.practice.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netcracker.practice.deserialization.AddressUnitDeserializer;
import com.netcracker.practice.serialization.AddressUnitSerializer;

import java.util.Set;
import java.util.UUID;
@JsonSerialize(using = AddressUnitSerializer.class)
@JsonDeserialize(using = AddressUnitDeserializer.class)
public class AddressUnit {


    private UUID addressUnitId;
    private Set<AddressUnitRecord> addressRecordsList;

    public UUID getAddressUnitGuId() {
        return addressUnitId;
    }

    public void setAddressUnitGuId(UUID addressUnitGuId) {
        this.addressUnitId = addressUnitGuId;
    }

    public Set<AddressUnitRecord> getAddressRecordsList() {
        return addressRecordsList;
    }

    public void setAddressRecordsList(Set<AddressUnitRecord> addressRecordsList) {
        this.addressRecordsList = addressRecordsList;
    }
    public AddressUnitRecord getLastAddressRecord(){
        for (AddressUnitRecord addressUnitRecord : addressRecordsList) {
            if(addressUnitRecord.getNextRecordId()==null){
                return addressUnitRecord;
            }
        }
        return null;
    }


}
