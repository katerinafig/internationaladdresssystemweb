package com.netcracker.practice.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netcracker.practice.deserialization.CharacteristicValueDeserializer;
import com.netcracker.practice.serialization.AddressUnitValueSerializer;
import com.netcracker.practice.serialization.CharacteristicValueSerializer;

import java.util.UUID;
@JsonSerialize(using = CharacteristicValueSerializer.class)
@JsonDeserialize(using = CharacteristicValueDeserializer.class)
public class CharacteristicValue {

    private UUID valueId;
    private String name;
    private String description;

    public UUID getValueId() {
        return valueId;
    }

    public void setValueId(UUID valueId) {
        this.valueId = valueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
