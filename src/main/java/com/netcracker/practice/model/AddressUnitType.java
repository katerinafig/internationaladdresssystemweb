package com.netcracker.practice.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netcracker.practice.deserialization.AddressUnitTypeDeserializer;
import com.netcracker.practice.serialization.AddressUnitSerializer;
import com.netcracker.practice.serialization.AddressUnitTypeSerializer;

import java.util.UUID;
@JsonSerialize(using = AddressUnitTypeSerializer.class)
@JsonDeserialize(using = AddressUnitTypeDeserializer.class)
public class AddressUnitType {

    private UUID addressUnitTypeId;
    private String name;
    private String description;
    private UUID parentType;

    public UUID getAddressUnitTypeId() {
        return addressUnitTypeId;
    }

    public void setAddressUnitTypeId(UUID addressUnitTypeId) {
        this.addressUnitTypeId = addressUnitTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getParentType() {
        return parentType;
    }

    public void setParentType(UUID parentTypeId) {
        this.parentType = parentTypeId;
    }
}
