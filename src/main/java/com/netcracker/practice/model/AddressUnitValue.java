package com.netcracker.practice.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netcracker.practice.deserialization.AddressUnitValueDeserializer;
import com.netcracker.practice.serialization.AddressUnitValueSerializer;

@JsonSerialize(using = AddressUnitValueSerializer.class)
@JsonDeserialize(using = AddressUnitValueDeserializer.class)
public class AddressUnitValue {

    private CharacteristicValue characterValue;
    private String value;

    public CharacteristicValue getCharacterValue() {
        return characterValue;
    }

    public void setCharacterValue(CharacteristicValue characterValue) {
        this.characterValue = characterValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
