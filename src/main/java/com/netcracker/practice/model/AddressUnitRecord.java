package com.netcracker.practice.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netcracker.practice.deserialization.AddressUnitRecordDeserializer;
import com.netcracker.practice.serialization.AddressUnitRecordSerializer;

import java.util.Set;
import java.util.UUID;
@JsonSerialize(using = AddressUnitRecordSerializer.class)
@JsonDeserialize( using = AddressUnitRecordDeserializer.class)
public class AddressUnitRecord {
    private UUID addressUnitRecordId;
    private String name;
    private AddressUnitType addressUnitType;
    private UUID addressUnit;
    private UUID parent;
    private UUID nextRecordId;
    private UUID prevRecordId;
    private Set<AddressUnitValue> addressUnitValueList;

    public UUID getParent() {
        return parent;
    }

    public void setParent(UUID parent) {
        this.parent = parent;
    }

    public AddressUnitType getAddressUnitType() {
        return addressUnitType;
    }
    public void setAddressUnitType(AddressUnitType addressUnitType) {
        this.addressUnitType = addressUnitType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getAddressUnitRecordId() {
        return addressUnitRecordId;
    }

    public void setAddressUnitRecordId(UUID addressUnitRecordId) {
        this.addressUnitRecordId = addressUnitRecordId;
    }

       public Set<AddressUnitValue> getAddressUnitValueList() {
        return addressUnitValueList;
    }

    public void setAddressUnitValueList(Set<AddressUnitValue> addressUnitValueList) {
        this.addressUnitValueList = addressUnitValueList;
    }

    public UUID getNextRecordId() {
        return nextRecordId;
    }

    public void setNextRecordId(UUID nextRecordId) {
        this.nextRecordId = nextRecordId;
    }

    public UUID getPrevRecordId() {
        return prevRecordId;
    }

    public void setPrevRecordId(UUID prevRecordId) {
        this.prevRecordId = prevRecordId;
    }

    public UUID getAddressUnit() {
        return addressUnit;
    }

    public void setAddressUnit(UUID addressUnit) {
        this.addressUnit = addressUnit;
    }
}
