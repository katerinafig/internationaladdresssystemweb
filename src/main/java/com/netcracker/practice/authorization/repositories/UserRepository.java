package com.netcracker.practice.authorization.repositories;

import com.netcracker.practice.authorization.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findByVkId(String vkId);

}
