package com.netcracker.practice.authorization.services;

import com.netcracker.practice.authorization.entities.User;
import com.netcracker.practice.authorization.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userFindByUsername = userRepository.findByUsername(username);

        if (userFindByUsername == null) {
            throw new UsernameNotFoundException(username);
        }
        UserDetails userDetails =
                new org.springframework.security.core.userdetails.User(userFindByUsername.getUsername(),
                        userFindByUsername.getPassword(),
                        userFindByUsername.getRoles());
        return userDetails;
    }
}
