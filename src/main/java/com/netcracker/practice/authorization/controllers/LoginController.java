package com.netcracker.practice.authorization.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class LoginController {
    @Autowired
    @Qualifier("oAuth2ClientContext")
    OAuth2ClientContext oAuth2ClientContext;

    @GetMapping("/login")
    public String login() {
        oAuth2ClientContext.setAccessToken(null);
        return "login";
    }

    @GetMapping("/")
    public String loginGoogle(Principal principal, HttpServletRequest request)
    {
        if(principal != null)
        {
            return "redirect:/location/root";
        }
        return "redirect:/login";
    }

}
