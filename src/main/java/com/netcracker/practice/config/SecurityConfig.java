package com.netcracker.practice.config;

import com.netcracker.practice.authorization.clientResources.ClientResources;
import com.netcracker.practice.authorization.repositories.UserRepository;
import com.netcracker.practice.authorization.services.UserService;
import com.netcracker.practice.authorization.services.google.UserInfoTokenServicesForGoogle;
import com.netcracker.practice.authorization.services.vk.UserInfoTokenServicesForVk;
import com.netcracker.practice.authorization.services.vk.VkCustomFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.CompositeFilter;

import javax.annotation.Resource;
import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableWebSecurity
@EnableOAuth2Client
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserService userService;
    @Autowired
    PasswordEncoder passwordEncoder;
//    @Autowired
//    @Qualifier("oAuth2ClientContext")
//    OAuth2ClientContext oAuth2ClientContext;
    @Autowired
    UserRepository userRepository;
    @Resource
    @Qualifier("accessTokenRequest")
    private AccessTokenRequest accessTokenRequest;

    @Bean
    @Qualifier("oAuth2ClientContext")
    @Scope(
            value = "session",
            proxyMode = ScopedProxyMode.INTERFACES
    )
    public OAuth2ClientContext oauth2ClientContext() {
        return new DefaultOAuth2ClientContext(this.accessTokenRequest);
    }

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/resources/**", "/", "/login/**", "/registration").permitAll()
                .and()
                .authorizeRequests().antMatchers("/h2-console/**").permitAll()
                .and().formLogin().loginPage("/login")
                .defaultSuccessUrl("/location/root", true).failureUrl("/login?error").permitAll()
                .and().logout().logoutSuccessUrl("/").permitAll();

//                .anyRequest().authenticated()
//                .and()
//                .oauth2Login();

        http.addFilterBefore(ssoFilter(), BasicAuthenticationFilter.class);
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

    @Bean
    public FilterRegistrationBean oAuth2ClientFilterRegistration(OAuth2ClientContextFilter oAuth2ClientContextFilter) {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(oAuth2ClientContextFilter);
        registration.setOrder(-100);
        return registration;
    }

    @Bean
    @ConfigurationProperties("google")
    public ClientResources google() {
        return new ClientResources();
    }

    @Bean
    @ConfigurationProperties("vk")
    public ClientResources vk() {
        return new ClientResources();
    }

    private Filter ssoFilter() {
        CompositeFilter filter = new CompositeFilter();
        List<Filter> filters = new ArrayList<>();
        OAuth2ClientAuthenticationProcessingFilter googleOAuth2ClientAuthenticationFilter =
                new OAuth2ClientAuthenticationProcessingFilter("/login/google");
        OAuth2RestTemplate googleOAuth2RestTemplate = new OAuth2RestTemplate(google().getClient(), oauth2ClientContext());
        googleOAuth2ClientAuthenticationFilter.setRestTemplate(googleOAuth2RestTemplate);
        UserInfoTokenServicesForGoogle tokenServicesGoogle = new UserInfoTokenServicesForGoogle(google().getResource().getUserInfoUri(), google().getClient().getClientId());
        tokenServicesGoogle.setRestTemplate(googleOAuth2RestTemplate);
        tokenServicesGoogle.setUserRepository(userRepository);
        googleOAuth2ClientAuthenticationFilter.setTokenServices(tokenServicesGoogle);
        filters.add(googleOAuth2ClientAuthenticationFilter);

        VkCustomFilter vkCustomFilter = new VkCustomFilter("/login/vk");
        OAuth2RestTemplate vkOAuth2RestTemplate = new OAuth2RestTemplate(vk().getClient(), oauth2ClientContext());
        vkCustomFilter.setRestTemplate(vkOAuth2RestTemplate);
        UserInfoTokenServicesForVk tokenServicesVk = new UserInfoTokenServicesForVk(vk().getResource().getUserInfoUri(), vk().getClient().getClientId());
        tokenServicesVk.setRestTemplate(vkOAuth2RestTemplate);
        tokenServicesVk.setUserRepository(userRepository);
        vkCustomFilter.setTokenServices(tokenServicesVk);
        filters.add(vkCustomFilter);

        filter.setFilters(filters);
        return filter;
    }

}
