package com.netcracker.practice.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.netcracker.practice.model.AddressUnit;
import com.netcracker.practice.model.AddressUnitRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class AddressUnitSerializer extends JsonSerializer<AddressUnit> {
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public void serialize(AddressUnit value, JsonGenerator jgen, SerializerProvider serializers) throws IOException {
        jgen.writeStartObject();
        if(value.getAddressUnitGuId()!=null)
        jgen.writeStringField("addressUnitId", value.getAddressUnitGuId().toString());
        jgen.writeArrayFieldStart("addressRecordsList");
        for (AddressUnitRecord addressUnitRecord : value.getAddressRecordsList()) {
            objectMapper.writeValue(jgen, addressUnitRecord);
        }
        jgen.writeEndArray();
        jgen.writeEndObject();
    }
}
