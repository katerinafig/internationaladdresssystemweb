package com.netcracker.practice.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.netcracker.practice.model.AddressUnitType;

import java.io.IOException;

public class AddressUnitTypeSerializer extends JsonSerializer<AddressUnitType> {
    @Override
    public void serialize(AddressUnitType value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        if(value.getAddressUnitTypeId()!=null)
        gen.writeStringField("addressUnitTypeId",value.getAddressUnitTypeId().toString());
        if(value.getName()!=null)
        gen.writeStringField("name",value.getName());
        if(value.getDescription()!=null)
        gen.writeStringField("description",value.getDescription());
        if (value.getParentType() != null)
            gen.writeStringField("parentType", value.getParentType().toString());
        gen.writeEndObject();

    }
}