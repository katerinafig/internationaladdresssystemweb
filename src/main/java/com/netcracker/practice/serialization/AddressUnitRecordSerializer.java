package com.netcracker.practice.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.netcracker.practice.model.AddressUnitRecord;
import com.netcracker.practice.model.AddressUnitValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class AddressUnitRecordSerializer extends JsonSerializer<AddressUnitRecord> {
    @Autowired
    ObjectMapper objectMapper;

    @Override
    public void serialize(AddressUnitRecord value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        if (value.getAddressUnitRecordId() != null)
            gen.writeStringField("addressUnitRecordId", value.getAddressUnitRecordId().toString());
        if (value.getName() != null)
            gen.writeStringField("name", value.getName());
        if (value.getAddressUnit() != null)
            gen.writeStringField("addressUnit", value.getAddressUnit().toString());
        if (value.getParent() != null)
            gen.writeStringField("parent", value.getParent().toString());
        if (value.getAddressUnitType() != null)
            gen.writeObjectField("addressUnitType", value.getAddressUnitType());
        if (value.getNextRecordId() != null)
            gen.writeStringField("nextRecordId", value.getNextRecordId().toString());
        if (value.getPrevRecordId() != null)
            gen.writeStringField("prevRecordId", value.getPrevRecordId().toString());
        if(value.getAddressUnitValueList()!=null) {
            gen.writeArrayFieldStart("addressUnitValueList");
            for (AddressUnitValue addressUnitValue : value.getAddressUnitValueList()) {
                if (addressUnitValue != null)
                    objectMapper.writeValue(gen, addressUnitValue);
            }
            gen.writeEndArray();
        }
        gen.writeEndObject();
    }
}
