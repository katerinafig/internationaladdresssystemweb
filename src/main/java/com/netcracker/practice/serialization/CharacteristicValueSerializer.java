package com.netcracker.practice.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.netcracker.practice.model.CharacteristicValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class CharacteristicValueSerializer extends JsonSerializer<CharacteristicValue> {
    @Autowired
    ObjectMapper objectMapper;
    @Override
    public void serialize(CharacteristicValue value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        if(value.getValueId()!=null)
        gen.writeStringField("valueId",value.getValueId().toString());
        if(value.getName()!=null)
        gen.writeStringField("name",value.getName());
        if(value.getDescription()!=null)
        gen.writeStringField("description",value.getDescription());
        gen.writeEndObject();

    }
}