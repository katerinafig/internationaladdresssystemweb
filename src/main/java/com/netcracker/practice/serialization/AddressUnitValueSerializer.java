package com.netcracker.practice.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.netcracker.practice.model.AddressUnitValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class AddressUnitValueSerializer extends JsonSerializer<AddressUnitValue> {
    @Autowired
    ObjectMapper objectMapper;
    @Override
    public void serialize(AddressUnitValue value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        if(value.getCharacterValue()!=null)
        gen.writeObjectField("characterValue", value.getCharacterValue());
        if(value.getValue()!=null)
        gen.writeStringField("value",value.getValue());
        gen.writeEndObject();

    }
}
