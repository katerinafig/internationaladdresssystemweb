package com.netcracker.practice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.practice.model.AddressUnit;
import com.netcracker.practice.model.AddressUnitRecord;
import com.netcracker.practice.model.AddressUnitValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.UUID;

@Controller
@RequestMapping("/location")
public class AddressUnitRecordsController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;
    private static final String ROOT_UNIT_ID = "ac65fba0-e36b-11e9-bf43-0242ac16012c";

    @GetMapping(value = "/{id}")
    public String getUnit(@PathVariable(name = "id") String id, Model model) throws IOException {
        AddressUnitRecord addressUnitRecord = new AddressUnitRecord();
        if (("root").equals(id)) {
            id = ROOT_UNIT_ID;
        }
        AddressUnit unit = objectMapper.readValue(restTemplate.getForObject(
                "http://localhost:8090/location/{id}", String.class, id),
                AddressUnit.class);
        addressUnitRecord.setParent(UUID.fromString(id));
        model.addAttribute("unit", unit);
        model.addAttribute("unitChildren", getUnitChildren(unit));
        model.addAttribute("record", addressUnitRecord);
        return "unit";
    }

    private AddressUnit[] getUnitChildren(AddressUnit addressUnit) throws IOException {
        return objectMapper.readValue(restTemplate.getForObject(
                "http://localhost:8090/location/{id}/children", String.class, addressUnit.getAddressUnitGuId().toString()),
                AddressUnit[].class);
    }

    @GetMapping(value = "/record/{id}")
    public String getValueRecord(@PathVariable(name = "id") String id, Model model) throws IOException {
        AddressUnitRecord unitRecord = objectMapper.readValue(restTemplate.getForObject(
                "http://localhost:8090/location/record/{id}", String.class, id),
                AddressUnitRecord.class);
        model.addAttribute("unitRecord", unitRecord);
        return "record";
    }

    @PostMapping(value = "/record/{id}")
    public String addValueRecord(@PathVariable(name = "id") String id, Model model) throws IOException {
        AddressUnitRecord unitRecord = objectMapper.readValue(restTemplate.getForObject(
                "http://localhost:8090/location/record/{id}", String.class, id),
                AddressUnitRecord.class);
        model.addAttribute("unitRecord", unitRecord);
        return "record";
    }

    @PostMapping("/{id}")
    public String createRecordPost(@PathVariable(name = "id") String id,@ModelAttribute(name = "record") AddressUnitRecord record, Model model) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddressUnitRecord> entity = new HttpEntity<>(record,headers);
        AddressUnitRecord unitRecord= restTemplate.postForObject("http://localhost:8090/location/createRecord",
                entity, AddressUnitRecord.class);
        if(unitRecord!=null) {
            model.addAttribute("unitRecord", unitRecord);
            return "record";
        }
        else return "unit";
    }
    @GetMapping("/createRecord")
    public String createRecordGer(@ModelAttribute(name = "record") AddressUnitRecord record, Model model) {
        model.addAttribute("record", record);
        return "new_unit";
    }
    @PostMapping("/createRecord")
    public String createRecordPost(@ModelAttribute(name = "record") AddressUnitRecord record, Model model) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<AddressUnitRecord> entity = new HttpEntity<>(record,headers);
        AddressUnitRecord unitRecord= restTemplate.postForObject("http://localhost:8090/location/createRecord",
                entity, AddressUnitRecord.class);
        if(unitRecord!=null) {
            model.addAttribute("error", "");
            model.addAttribute("unitRecord", unitRecord);
            return "record";
        }
        else {
            model.addAttribute("error", "Объект с таким именем уже существует");
            return "new_unit";
        }
    }
}